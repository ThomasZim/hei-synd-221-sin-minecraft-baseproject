## Introduction

This project was created to introduce modbus protocol, databases and web pages to HES-SO students. This java program is used to connect the minecraft world you already downloaded with the webpage and the database. It also provides a SmartController component which tries to maximize factory energy production.

## Run executable

Java executable is located in directory out/artifacts/Minecraft_jar
To run it properly, you need to navigate to the folder of the executable with cd ...out/artifacts/Minecraft_jar
then, use command : java -jar Minecraft.jar dbHostName dbUserNAme modbusTcpHost modbusTcpPort
recommanded params are : java -jar Minecraft.jar localhost labo localhost 1502 . Remember that minecraft world has to be running first.

## Javadoc

Javadoc is available in javadoc/hei directory.

## Authors and contributors

* **Thomas Zimmermann** - *Core, Database and Webpage developper*
* **Valérie Pompili** - *Smart Controller and Field (Modbus) developper*

