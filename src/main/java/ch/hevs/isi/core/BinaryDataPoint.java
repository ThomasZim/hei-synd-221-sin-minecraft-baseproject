package ch.hevs.isi.core;

import ch.hevs.isi.database.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

/**
 * Represents a datapoint that stores a boolean value
 *
 * @author Zimmermann, Pompili
 * @version v1.2
 */
public class BinaryDataPoint extends DataPoint{
    // Attributes
    private boolean value;

    /**
     * Public constructor.
     * Creates a datapoint by giving key values
     * to the superclass.
     * @param label     The reference label
     * @param isOutput  If the datapoint is an output
     */
    public BinaryDataPoint(String label, boolean isOutput) {
        super(label, isOutput);
    }

    /**
     * Set the boolean value of the datapoint.
     * Note: It informs the database, the web
     * and the field by calling their onNewValue
     * methods.
     * @param value     The new datapoint value
     */
    public void setValue(boolean value)
    {
        this.value = value;

        DatabaseConnector dc = DatabaseConnector.getInstance();
        WebConnector wc = WebConnector.getInstance();

        dc.onNewValue(this);
        wc.onNewValue(this);

        if(this.isOutput())
        {
            FieldConnector fc = FieldConnector.getInstance();
            fc.onNewValue(this);
        }
    }

    /**
     * Get the value of the datapoint.
     * @return      The value
     */
    public boolean getValue()
    {
        return value;
    }

    /**
     * Get a string version of the data
     * contained in the datapoint.
     * @return
     */
    @Override
    public String toString() {
        return "Binary data point " + getLabel() + " has the value " + value;
    }

    /**
     * Change the value of the boolean
     * datapoint using a string.
     * @param s     The string value
     * @return      The boolean value
     */
    @Override
    public Boolean fromString(String s) {
        if (s.equals("true")){
            this.setValue(true);
            System.out.println("Oeoeoeoeoe");
            return true;
        }
        else{
            if(s.equals("false")){
                this.setValue(false);
                System.out.println("Nononono");
                return true;
            }
            else{
                return false;
            }
        }

    }

}
