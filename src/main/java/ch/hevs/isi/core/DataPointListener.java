package ch.hevs.isi.core;

/**
 * Interface that declares every methods
 * essentials to broadcast datapoint new
 * values.
 */
public interface DataPointListener {
    void onNewValue(FloatDataPoint fdp);
    void onNewValue(BinaryDataPoint fdp);
}
