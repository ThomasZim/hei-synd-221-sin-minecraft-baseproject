package ch.hevs.isi.core;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Creates a datapoint with contains an identification
 * label matching with either a boolean or a float
 * value.
 *
 * @author Zimmermann, Pompili
 * @version v1.7
 */
public abstract class DataPoint {

    // Attributes
    private String label;
    private boolean isOutput;
    private static Map<String, DataPoint> dataPointMap = new HashMap<>();

    /**
     * Protected constructor.
     * Creates a datapoint using the provided
     * information and saves it in the static
     * hashmap for further retrieves.
     * @param label     Key label
     * @param isOutput  If it's an output
     */
    protected DataPoint(String label, boolean isOutput) {
        dataPointMap.put(label, this);
        this.label = label;
        this.isOutput = isOutput;
    }

    /**
     * Retrieves the datapoint using its label
     * @param label     The datapoint label
     * @return          The corresponding datapoint
     */
    public static DataPoint getDataPointFromLabel(String label) {
        return dataPointMap.get(label);
    }

    /**
     * Get the datapoint label
     * @return      The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Is the datapoint an output?
     * @return  The answer to the question above
     */
    public boolean isOutput()
    {
        return isOutput;
    }

    /**
     * Abstract method. Redefined in child classes.
     */
    public abstract String toString();

    /**
     * Abstract method. Redefined in child classes.
     */
    public abstract Boolean fromString(String s);

    /**
     * Main method. Used for test purpose
     */
    public static void main(String[] args) {

        FloatDataPoint fdp1 = new FloatDataPoint("BATT_CHRG_FLOAT",true);
        FloatDataPoint fdp2 = new FloatDataPoint("BATT_P_FLOAT",false);
        FloatDataPoint fdp3 = new FloatDataPoint("REMOTE_COAL_SP", false);

        float counter = 0;

        while(true) {

            fdp1.setValue(counter);
            counter += 0.01;
            try {
                TimeUnit.SECONDS.sleep(2);
            }catch(InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }


}
