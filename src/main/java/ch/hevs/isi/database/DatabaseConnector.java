package ch.hevs.isi.database;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.utils.Utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.concurrent.TimeUnit;

/**
 * Used to update InfluxDB database
 * using new datapoint values.
 *
 * @author Zimmermann
 */
public class DatabaseConnector implements DataPointListener {
    // Attributes
    private static DatabaseConnector instance;

    /**
     * Private constructor.
     */
    private DatabaseConnector()
    {

    }

    /**
     * Get the only instance of this class.
     * @return  The only instance
     */
    public static DatabaseConnector getInstance()
    {
        if(instance==null)
        {
            instance = new DatabaseConnector();
        }
        return instance;
    }

    /**
     * Push the given values to the database
     * @param label     The label of the datapoint
     * @param value     Its string represented value
     */
    private void pushToDatabase(String label, String value) {
        HttpURLConnection connection = null;
        URL url = null;

        try {
            url = new URL("https://influx.sdi.hevs.ch/write?db=SIn36");
            connection = (HttpURLConnection) url.openConnection();
            String userpass = "SIn36:"  + Utility.md5sum("SIn36");
            String encoding = Base64.getEncoder().encodeToString(userpass.getBytes());
            connection.setRequestProperty("Authorization", "Basic " + encoding);
            connection.setRequestProperty("Content-Type","binary/octet-stream");
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            OutputStreamWriter writer = null;

            writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(label + " value=" + value);
            writer.flush();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            int responseCode = connection.getResponseCode();
            while((in.readLine())!=null)
            {

            }
            connection.disconnect();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Is called on Datapoint modification.
     * @param fdp   The modified datapoint
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) {
        pushToDatabase(fdp.getLabel(),Float.toString(fdp.getValue()));
    }

    /**
     * Is called on Datapoint modification.
     * @param bdp   The modified datapoint
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        float valueToSend;
        if(bdp.getValue())
        {
            valueToSend = 1;
        }
        else
        {
            valueToSend = 0;
        }

        pushToDatabase(bdp.getLabel(),Float.toString(valueToSend));
    }

    /**
     * Main method. Used for test purpose
     */
    public static void main(String[] args) {
        FloatDataPoint fdp1 = new FloatDataPoint("BATT_CHRG_FLOAT",true);
        float counter = 0;

        while(true) {

            fdp1.setValue(counter);
            counter += 0.01;
            try {
                TimeUnit.SECONDS.sleep(2);
            }catch(InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
}
