package ch.hevs.isi.smartcontroller;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.web.WebConnector;

/**
 * Control the Minecraft world in
 * order to regulate the battery
 * charge.
 *
 * @author Zimmermann, Pompili
 * @version 2.0
 */
public class SmartController {
    private static SmartController instance;

    /**
     * Private constructor.
     */
    private SmartController(){

    }

    /**
     * Get the only instance of this class
     * @return  The only instance
     */
    public static SmartController getInstance()
    {
        if(instance==null)
        {
            instance = new SmartController();
        }
        return instance;
    }

    /**
     * Do the regulation process.
     */
    public static void doControl()
    {
        // Get the useful outputs
        FloatDataPoint factoryPower = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");
        BinaryDataPoint remoteSolar = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");
        BinaryDataPoint remoteWind = (BinaryDataPoint) BinaryDataPoint.getDataPointFromLabel("REMOTE_WIND_SW");
        FloatDataPoint coalSetUp = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
        FloatDataPoint batteryLevel = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");
        FloatDataPoint clock = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("CLOCK_FLOAT");
        FloatDataPoint score = (FloatDataPoint) FloatDataPoint.getDataPointFromLabel("SCORE");

        //Preshot 19h peak
        if (clock.getValue()>0.7f && clock.getValue()<0.833f && batteryLevel.getValue()<0.97f){
            remoteSolar.setValue(true);
            remoteWind.setValue(true);
            coalSetUp.setValue(0.8f);
            factoryPower.setValue(0.3f);
        } // Battery full
        else if (batteryLevel.getValue() >= 0.97f) {
            remoteSolar.setValue(false);
            remoteWind.setValue(false);
            coalSetUp.setValue(0f);
            factoryPower.setValue(1f);

        } // Battery almost full
        else if (batteryLevel.getValue() > 0.7f && batteryLevel.getValue() < 0.97f) {
            remoteSolar.setValue(true);
            remoteWind.setValue(true);
            coalSetUp.setValue(0.1f);
            factoryPower.setValue(1f);

        } // Battery level between 50% and 70% at day
        else if (batteryLevel.getValue() > 0.5f && batteryLevel.getValue() <= 0.7f && clock.getValue()>0.25f && clock.getValue()<=0.6f) {
            remoteSolar.setValue(true);
            remoteWind.setValue(true);
            coalSetUp.setValue(0f);
            factoryPower.setValue(0.7f);//0.5f

        } // Battery level between 50% and 70% at night
        else if (batteryLevel.getValue() > 0.5f && batteryLevel.getValue() <= 0.7f && (clock.getValue()<=0.25f || clock.getValue()>0.6f)) {
            remoteSolar.setValue(true);
            remoteWind.setValue(true);
            coalSetUp.setValue(0.4f);
            factoryPower.setValue(0.3f);//0.3f

        } // Battery level between 30% and 50% at day
        else if (batteryLevel.getValue() > 0.3f && batteryLevel.getValue() <= 0.5f && clock.getValue()>0.25f && clock.getValue()<=0.6f) {
            remoteSolar.setValue(true);
            remoteWind.setValue(true);
            coalSetUp.setValue(0.2f);
            factoryPower.setValue(0.3f);//0.3f

        } // Battery level between 30% and 50% at night
        else if (batteryLevel.getValue() > 0.3f && batteryLevel.getValue() <= 0.5f && (clock.getValue()<=0.25f || clock.getValue()>0.6f)) {
            remoteSolar.setValue(true);
            remoteWind.setValue(true);
            coalSetUp.setValue(0.5f);
            factoryPower.setValue(0.4f);//0.2f

        } // Battery level lower than 30%
        else {
            remoteSolar.setValue(true);
            remoteWind.setValue(true);
            coalSetUp.setValue(0.8f);
            factoryPower.setValue(0f);
        }
            System.out.println(score);

    }
}
