package ch.hevs.isi;

import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.field.PollTask;
import ch.hevs.isi.utils.Utility;

import java.util.Timer;

public class MinecraftController {
    public static void usage() {
        System.out.println("Parameters: <localhost> <labo> <localhost> <1502> [-modbus4j]");
        System.exit(1);
    }

    @SuppressWarnings("all")
    public static void main(String[] args) {

        // ------------------------------------- DO NOT CHANGE THE FOLLOWING LINES -------------------------------------
        String dbHostName    = "localhost";
        String dbName        = "labo";
        String dbUserName    = "SIn36";
        String dbPassword    = "root";

        String modbusTcpHost = "localhost";
        int    modbusTcpPort = 1502;

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;


        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length == 4) {
            parameters = args;

            // Decode parameters for influxDB
            dbHostName  = parameters[0];
            dbUserName  = parameters[1];
            dbName      = dbUserName;
            dbPassword  = Utility.md5sum(dbUserName);

            // Decode parameters for Modbus TCP
            modbusTcpHost = parameters[2];
            modbusTcpPort = Integer.parseInt(parameters[3]);
        } else {
            System.out.println("error");
            usage();
        }

        // ------------------------------------ /DO NOT CHANGE THE FOLLOWING LINES -------------------------------------

        FieldConnector fc = FieldConnector.getInstance();
        fc.loadRegister("ModbusMap.csv");
        Timer pollTimer = new Timer();
        long period = 10000;
        pollTimer.scheduleAtFixedRate(new PollTask(),0,period);


    }
}
