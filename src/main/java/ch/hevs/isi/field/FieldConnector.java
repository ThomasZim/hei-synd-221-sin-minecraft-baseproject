package ch.hevs.isi.field;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.utils.Utility;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Timer;

/**
 * Connect the inner part of the program
 * to the field, but without being
 * too much specified. (i.e., the field
 * can be whatever we want).
 */
public class FieldConnector implements DataPointListener {
    // Attributes
    private static FieldConnector instance;
    private static final int ON = 0xff00;        //size of the header (fixed)
    private static final int OFF = 0x0000;        //size of the header (fixed)

    /**
     * Private constructor.
     */
    private FieldConnector()
    {

    }

    /**
     * Get the only instance of the program
     * @return  The only instance
     */
    public static FieldConnector getInstance()
    {
        if(instance==null)
        {
            instance = new FieldConnector();
        }
        return instance;
    }

    /**
     * Push the specified values to the field.
     * @param label     The datapoint label
     * @param value     Its value (in string)
     */
    private void pushToField(String label, String value)
    {

    }

    /**
     * Is called on Datapoint modification.
     * @param fdp   The modified datapoint
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) {
        try
        {
            Register.getRegisterFromDataPoint(fdp).write();
        }
     catch (IOException | ModbusException e) {
        e.printStackTrace();
    }
        pushToField(fdp.getLabel(),Float.toString(fdp.getValue()));
    }

    /**
     * Is called on Datapoint modification.
     * @param bdp   The modified datapoint
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        try
        {
            Register.getRegisterFromDataPoint(bdp).write();
        }
        catch (IOException | ModbusException e) {
            e.printStackTrace();
        }
        pushToField(bdp.getLabel(),Boolean.toString(bdp.getValue()));
    }

    /**
     * Load register from the CSV file (values separated by ';')
     * @param path      The path to the file
     */
    public static void loadRegister(String fileName){
        //Matching column index
        final int labelC = 0, typeC = 1, inputC = 2, outputC = 3, addressC = 4, rangeC = 5, offsetC = 6;
        final String splitSymbol = ";";

        try {
            String label;
            char type;
            boolean isOutput;
            int address, range, offset;

            String line;
            String[] splited;
            boolean firstLine = true;

            //Create a new reader
            BufferedReader br = Utility.fileParser(null,fileName);

            //Load register as until end of file
            while ((line = br.readLine()) != null) {
                if(firstLine){  //Skip if first line
                    firstLine = false;
                    continue;
                }

                //Split line in an array of string
                splited = line.split(splitSymbol);

                //Retrieve label and type
                label = splited[labelC];
                type = splited[typeC].charAt(0);

                //Is the register an output or an input?
                if(splited[outputC].equals("Y"))
                    isOutput = true;
                else
                    isOutput = false;

                //Read Modbus address, its range and offset values
                address = Integer.valueOf(splited[addressC]);
                range = Integer.valueOf(splited[rangeC]);
                offset = Integer.valueOf(splited[offsetC]);

                //Create corresponding register
                if(type == 'F')
                    new FloatRegister(address, range, offset, label, isOutput);
                else
                   new BooleanRegister(address, range, offset, label, isOutput);

                System.out.println("Loading register " + label);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Done !\n");
    }

    /**
     * Main method. Used for test purpose.
     */
    public static void main(String[] args) throws IOException, ModbusException {


        Timer pollTimer = new Timer();
        long period = 10000;

        pollTimer.scheduleAtFixedRate(new PollTask(),0,period);


        //ModbusAccessor clientTest = ModbusAccessor.getInstance();
        //clientTest.readCoils((short)609,(short)1);
        //clientTest.readHoldingRegister((short)57 ,(short)2);
        //clientTest.writeSingleCoil((short)401,(short) OFF);
        //clientTest.writeMultipleRegisters((short)205,(short)2,0.5f);
    }
}
