package ch.hevs.isi.field;



import ch.hevs.isi.utils.Utility;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

/**
 * Sends and receives frames from the
 * Modbus server using the transaction
 * class. Reads and writes coils and
 * registers.
 *
 * @author Pompili
 * @version 2.4
 */
public class ModbusAccessor  extends Socket {
    private InputStream in;
    private  OutputStream out;
    private static ModbusAccessor instance;

    private static final int ON = 0xff00;        //size of the header (fixed)
    private static final int OFF = 0x0000;        //size of the header (fixed)

    /**
     * Private constructor.
     * Open a TCPIP connection between
     * the program and the Modbus server.
     * @param serverName    The server host name
     * @param portNumber    The port number
     * @throws IOException
     */
    private ModbusAccessor(String serverName, int portNumber) throws IOException {
        super(serverName, portNumber);
        in = getInputStream();
        out = getOutputStream();
    }

    /**
     * Get the only instance of this class
     * @return  The only instance
     * @throws IOException
     */
    public static ModbusAccessor getInstance() throws IOException {
        if(instance==null)
        {
            instance = new ModbusAccessor("localHost", 1502);
        }
        return instance;
    }

    /**
     * Read a quantity of coils at the specified
     * address
     * @param ad                The starting address
     * @param quantityCoils     The quantity of coil to read
     * @return                  The value of the coil
     * @throws ModbusException
     */
    public boolean readCoils(short ad, short quantityCoils) throws ModbusException {
        //Request
        //Function Code         1 Byte  0x01
        //Starting Address      2 Bytes
        //Quantity of Coil      2 Bytes
        //Length (for MBAP)     1+2+2+1=6 Bytes

        MBAP cmbap = new MBAP((short)(1),(short) 0,(short)6,(byte)1);           //create the header
        PDU cpdu = new PDU((byte)0x01, cmbap.getlength());                      //create empty pdu with function code
        cpdu.getBB().putShort(1,(short)ad).putShort( 3,quantityCoils);       //fill the pdu


        Frame request = new Frame(cmbap,cpdu);
        Transaction t = new Transaction(in,out);                //send the request

        byte[] response = t.process(request);
        Frame answer = new Frame(response);

        answer.getPDU().getBB().position(0);            //reset index


        //check error (1 byte function code error and 1 byte for exception code)
        if(answer.getPDU().getBB().get()!=0x01) throw(new ModbusException(answer.getPDU().getBB().get()));


        //create the boolean tab to return
        boolean valBack;
        answer.getPDU().getBB().position(2);

            if(answer.getPDU().getBB().get()==0)
            valBack=false;
            else
                valBack=true;




        return valBack;


    }

    /**
     * Write a single coil on the Modbus
     * server.
     * @param ad        The address of the coil
     * @param value     Its value
     * @throws ModbusException
     */
    public void writeSingleCoil(short ad, short value) throws ModbusException {
        //Request
        //Function Code         1 Byte  0x05
        //Starting Address      2 Bytes
        //Output Value          2 Bytes
        //Length (for MBAP)     1+2+2+1=6 Bytes

        //prepare the message to send
        MBAP cmbap = new MBAP((short)(1),(short) 0,(short)6,(byte)1);           //create Header
        PDU cpdu = new PDU((byte)0x05, cmbap.getlength());                      //create empty PDU
        cpdu.getBB().putShort(1,(short)ad).putShort(3,(short)value);       //fill PDU with value
        Frame request = new Frame(cmbap,cpdu);

        //create the frame
        Transaction t = new Transaction(in,out);                                //create a transaction
        byte[] response = t.process(request);                                   //send message
        Frame answer = new Frame(response);                                     //translate the answer in a frame

        answer.getPDU().getBB().position(0);
        if(answer.getPDU().getBB().get()==0x85) throw(new ModbusException(answer.getPDU().getBB().get()));


    }

    /**
     * Read a holding register at the specified
     * address.
     * @param ad            The register address
     * @param nbrRegister   The number of register to read
     * @return              The value of the registers (in float)
     * @throws ModbusException
     */
    public float readHoldingRegister(short ad, short nbrRegister) throws ModbusException {
        //Prepare Request
        //Function Code         1 Byte  0x03
        //Starting Address      2 Bytes
        //Quantity of Registers 2 Bytes
        //Length (for MBAP)     1+2+2+1=6 Bytes

        //prepare the message to send
        MBAP cmbap = new MBAP((short)(1),(short) 0,(short)6,(byte)1);           //create the header
        PDU cpdu = new PDU((byte)0x03, cmbap.getlength());                      //create empty pdu with function code
        cpdu.getBB().putShort(1,(short)ad).putShort( 3, nbrRegister);       //fill the pdu
        Frame request = new Frame(cmbap,cpdu);
        //send the trame and wait the answer
        Transaction t = new Transaction(in,out);
        byte[] response = t.process(request);
        Frame answer = new Frame(response);
        //check the exceptions
        answer.getPDU().getBB().position(0);
        if(answer.getPDU().getBB().get()!=0x03) throw(new ModbusException(answer.getPDU().getBB().get()));


        //extract the value
        float valFloat;
        answer.getPDU().getBB().position(2);

        //!!!!Pay attention, we read float so we read two registers for one data!!!!

            valFloat= answer.getPDU().getBB().getFloat();

        return valFloat;
    }

    /**
     * Write multiple holding registers
     * @param ad            The starting address
     * @param nbrRegister   The number of registers
     * @param value         The value to write
     * @throws ModbusException
     */
    public void writeMultipleRegisters(short ad,short nbrRegister, float value) throws ModbusException{
        //Prepare Request
        //Function Code         1 Byte      0x10
        //Starting Address      2 Bytes
        //Nbr Registers (N)     2 Bytes
        //Byte Count            1 Byte      2*N
        //Value of Registers    N*2 Bytes
        //total length          7+2*nbrRegister
        int length = 7+2*nbrRegister;

        //prepare the message to send
        MBAP cmbap = new MBAP((short)(1),(short) 0,(short)length,(byte)1);           //create Header
        PDU cpdu = new PDU((byte)0x10, cmbap.getlength());                      //create empty PDU
        int convVal = Float.floatToIntBits(value);
        cpdu.getBB().position(1);
        cpdu.getBB().putShort((short)ad).putShort((short)nbrRegister).put((byte)(2*nbrRegister)).putInt(convVal);       //fill PDU with value
        Frame request = new Frame(cmbap,cpdu);

        //create the frame
        Transaction t = new Transaction(in,out);                                //create a transaction
        byte[] response = t.process(request);                                   //send message
        Frame answer = new Frame(response);                                     //translate the answer in a frame


        answer.getPDU().getBB().position(0);
        if(answer.getPDU().getBB().get()==0x90) throw(new ModbusException(answer.getPDU().getBB().get()));
        ByteBuffer bbcomp = ByteBuffer.allocate(5);
        bbcomp.put((byte)0x10).putShort((short)ad).putShort((short)nbrRegister);


    }

    /**
     * Main method. Used for test purpose
     */
    public static void main(String[] args) {
        ModbusAccessor clientTest = null;

        try {
            clientTest = new ModbusAccessor("localHost", 1502);
            //clientTest.readCoils((short)609,(short)1);
            //clientTest.readHoldingRegister((short)57 ,(short)2);
            //clientTest.writeSingleCoil((short)401,(short) OFF);
            clientTest.writeMultipleRegisters((short)205,(short)2,0.0f);
        } catch (IOException | ModbusException e) {
            e.printStackTrace();
        }

    }

}


