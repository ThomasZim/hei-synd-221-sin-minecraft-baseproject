package ch.hevs.isi.field;

import ch.hevs.isi.smartcontroller.SmartController;

import java.io.IOException;
import java.util.TimerTask;

/**
 * Is called by a thread to perform
 * a read on all the registers.
 *
 * @author Pompili
 * @version v1.1
 */
public class PollTask extends TimerTask {

    /**
     * Public constructor.
     */
    public PollTask(){

    }

    /**
     * Perform the read iteration
     */
    @Override
    public void run() {
        try{
            Register.poll();
            SmartController.getInstance().doControl();
        }
        catch(IOException | ModbusException e){
            e.printStackTrace();
        }

    }
}
