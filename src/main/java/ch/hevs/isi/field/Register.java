package ch.hevs.isi.field;


import ch.hevs.isi.core.DataPoint;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Creates a register storing every useful
 * information concerning a true Modbus
 * register, in addition to a reference
 * to a Datapoint corresponding to this
 * information
 *
 * @author Pompili
 * @version v1.4
 */
public abstract class Register {
    protected int address, range, offset;
    protected boolean isOutput;
    protected String label;
    protected static Map<DataPoint, Register> registerMap = new HashMap<>();

    /**
     * Creates a register using the
     * provided information
     * @param address   The Modbus register address
     * @param range     Its range
     * @param offset    Its offset
     * @param label     Its reference label
     * @param isOutput  If it's an output
     */
    protected Register(int address,int range, int offset, String label, boolean isOutput) {
        this.address = address;
        this.range = range;
        this.offset = offset;
        this.label = label;
        this.isOutput = isOutput;
    }

    /**
     * Retrieves the register corresponding to
     * the given datapoint.
     * @param dataP     The datapoint
     * @return          The register
     */
    public static Register getRegisterFromDataPoint(DataPoint dataP) {
        return registerMap.get(dataP);
    }

    /**
     * Abstract method. See child classes.
     * @throws IOException
     * @throws ModbusException
     */
    public abstract void write() throws IOException, ModbusException;

    /**
     * Abstract method. See child classes.
     * @throws IOException
     * @throws ModbusException
     */
    public  abstract void read() throws IOException, ModbusException;

    /**
     * Read all register on PollTask call.
     * @throws IOException
     * @throws ModbusException
     */
    public static void poll() throws IOException, ModbusException {

        for(Register tempRegister: registerMap.values()){
           tempRegister.read();
        }
    }

}
