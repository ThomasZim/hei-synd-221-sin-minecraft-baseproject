package ch.hevs.isi.field;

import ch.hevs.isi.utils.Utility;

import java.nio.ByteBuffer;

/**
 * Creates a Modbus frame containing a
 * header and a message, ready to send
 * on the output stream
 *
 * @author Pompili
 * @version v1.3
 */
public class Frame {
    private static final int HeaderSize = 7;        //size of the header (fixed)


    private MBAP mbap;
    private PDU pdu;
    private ByteBuffer bb;


    /**
     * Public constructor.
     * Create a frame using a MBAP and
     * a PDU. It also fill the ByteBuffer.
     * @param a     The MBAP
     * @param b     The PDU
     */
    public Frame(MBAP a, PDU b){
        mbap = a;
        pdu = b;
        //create BB buffer from concat of mbap and pdu
        bb = ByteBuffer.allocate(mbap.getBB().capacity()+pdu.getBB().capacity());        //allocate the size of bb
        byte[] tab1 = mbap.getBB().array();
        byte[] tab2 = pdu.getBB().array();
        byte[] concatenation = new byte[mbap.getBB().capacity()+pdu.getBB().capacity()];
        for(int i = 0;i<HeaderSize;i++)
        {
            concatenation[i]=tab1[i];
        }
        for(int j = 0;j<pdu.getBB().capacity();j++)
        {
            concatenation[j+HeaderSize]=tab2[j];
        }
        bb = ByteBuffer.wrap(concatenation);



    }

    /**
     * Public constructor.
     * Creates a MBAP and a PDU using
     * only the provided byte array.
     * @param tab   The byte array
     */
    public Frame(byte[] tab){
        bb = ByteBuffer.wrap(tab);

        mbap  = new MBAP(bb);

        byte[] temp = new byte[tab.length-HeaderSize];
        for(int i = 0; i<(tab.length-HeaderSize);i++)
        {
            temp[i]=tab[HeaderSize+i];
        }
        //Frame response = new Frame(ByteBuffer.wrap(answer));
        pdu = new PDU(ByteBuffer.wrap(temp));


    }

    /**
     * Get the ByteBuffer
     * @return  the ByteByffer
     */
    public ByteBuffer getBB(){
        return bb;
    }

    /**
     * Get the MPAB
     * @return  The MBAP
     */
    public MBAP getMbap(){
        return mbap;
    }

    /**
     * Get the PDU
     * @return  The PDU
     */
    public PDU getPDU(){
        return pdu;
    }

}
