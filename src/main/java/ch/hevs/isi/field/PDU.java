package ch.hevs.isi.field;

import java.nio.ByteBuffer;

/**
 * This is a Modbus message.
 *
 * @author Pompili
 * @version v1.2
 */
public class PDU {
    private byte functionCode;
    private ByteBuffer pduBB;

    /**
     * Public constructor.
     * Creates a message with a function code and
     * the message length.
     * @param a     The function code
     * @param size  The message length
     */
    public PDU(byte a, int size){
        functionCode = a;
        pduBB = ByteBuffer.allocate(size-1);
        pduBB.put(a);
    }

    /**
     * Public constructor.
     * @param bb    The ByteBuffer containing the message
     */
    public PDU(ByteBuffer bb){
        pduBB = ByteBuffer.allocate(bb.capacity()).put(bb);
        functionCode = pduBB.get(0);
    }

    /**
     * Get the message function code
     * @return  The function code
     */
    public byte getFunctionCode(){
        return functionCode;
    }

    /**
     * Get the function code from the ByteBuffer.
     * @param bb    The ByteBuffer
     */
    public void fromBB(ByteBuffer bb){
        functionCode = bb.get();
    }

    /**
     * Get the ByteBuffer
     * @return  The message ByteBuffer
     */
    public ByteBuffer getBB(){
        return pduBB;
    }
}
