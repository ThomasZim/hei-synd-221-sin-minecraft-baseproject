package ch.hevs.isi.field;


import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.io.IOException;

/**
 * Implements a register that stores a
 * float value.
 *
 * @author Pompili
 * @version v2.1
 */
public class FloatRegister extends Register{
    private FloatDataPoint dataPoint;

    /**
     * Public constructor.
     * Create a new register by passing arguments to
     * the superclass and create a new datapoint.
     * @param address   The Modbus address of the register
     * @param range     Its range
     * @param offset    Its offset
     * @param label     Its key label
     * @param isOutput  If it's an output
     */
    public FloatRegister(int address,int range, int offset, String label, boolean isOutput){

        super(address, range, offset, label, isOutput);
        dataPoint = new FloatDataPoint(label,isOutput);
        Register.registerMap.put(dataPoint,this);

    }

    /**
     * Read the register at the stored address
     * on the Modbus server.
     * @throws IOException
     * @throws ModbusException
     */
    public void read() throws IOException, ModbusException {
        ModbusAccessor mbAcc =  ModbusAccessor.getInstance();
        float value = mbAcc.readHoldingRegister((short)address,(short)2);
        value = value * range + offset;
        dataPoint.setValue(value);

    }

    /**
     * Write the datapoint value to the Modbus
     * server.
     * @throws IOException
     * @throws ModbusException
     */
    public void write() throws IOException, ModbusException {
        float value = dataPoint.getValue();

        value = (value-offset)/range;
        ModbusAccessor mbAcc =  ModbusAccessor.getInstance();
        mbAcc.writeMultipleRegisters((short)address,(short)2,value);
    }
}
