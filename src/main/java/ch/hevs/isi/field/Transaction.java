package ch.hevs.isi.field;

import ch.hevs.isi.utils.Utility;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 * Low level class used to communicate
 * directly with the Modbus server using
 * the provided input and output streams.
 *
 * @author Pompili
 * @version v1.3
 */
public class Transaction {
    private static InputStream is;
    private static OutputStream os;

    /**
     * Public constructor.
     * Saves the given output and input
     * streams.
     * @param in    The input stream
     * @param out   The output stream
     */
    public Transaction(InputStream in, OutputStream out) {
        is = in;
        os = out;
    }

    /**
     * Process the transaction
     * @param f     The frame to send
     * @return      The received byte array
     */
    public byte[] process(Frame f){

        byte[] answer;

        try{

            Utility.sendBytes( os, f.getBB().array(),0,f.getBB().capacity());
            while((answer = Utility.readBytes(is)).length<7){}

        }
        catch(IOException e){
            e.printStackTrace();
            return null;
        }


        return  answer;
    }


}


