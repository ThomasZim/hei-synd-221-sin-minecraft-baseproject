package ch.hevs.isi.field;


import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.FloatDataPoint;


import java.io.IOException;

/**
 * Implements a register that stores a
 * boolean value.
 *
 * @author Pompili
 * @version v2.1
 */
public class BooleanRegister extends Register {

    private BinaryDataPoint dataPoint;

    /**
     * Public constructor.
     * Create a new register by passing arguments to
     * the superclass and create a new datapoint.
     * @param address   The Modbus address of the register
     * @param range     Its range
     * @param offset    Its offset
     * @param label     Its key label
     * @param isOutput  If it's an output
     */
    public BooleanRegister(int address,int range, int offset, String label, boolean isOutput){
        super(address, range, offset, label, isOutput);
        dataPoint = new BinaryDataPoint(label,isOutput);
        registerMap.put(dataPoint,this);

    }

    /**
     * Read the register at the stored address
     * on the Modbus server.
     * @throws IOException
     * @throws ModbusException
     */
    public void read() throws IOException, ModbusException {
        ModbusAccessor mbAcc =  ModbusAccessor.getInstance();
        boolean value;
        value = mbAcc.readCoils((short)address,(short)1);
        dataPoint.setValue(value);
    }

    /**
     * Write the datapoint value to the Modbus
     * server.
     * @throws IOException
     * @throws ModbusException
     */
    public void write() throws IOException, ModbusException {
        boolean value = dataPoint.getValue();
        ModbusAccessor mbAcc =  ModbusAccessor.getInstance();
        mbAcc.writeSingleCoil((short)address, value ? (short)0xff00:(short) 0);

    }


}
