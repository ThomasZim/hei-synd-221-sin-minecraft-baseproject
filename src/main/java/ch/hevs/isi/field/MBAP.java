package ch.hevs.isi.field;

import java.nio.ByteBuffer;

/**
 * Implements a Modbus Header containing
 * every useful information.
 *
 * @author Pompili
 * @version v1.1
 */
public class MBAP {
    private short tID;      //transaction ID
    private short pID;      //protocole ID = 0
    private short lenght;
    private byte uID;       //unit ID = 1;
    private ByteBuffer mbapBB;

    /**
     * Public constructor.
     * Create a MBAP using the provided
     * arguments.
     * @param a     The transaction ID
     * @param b     The protocol ID
     * @param c     The message length
     * @param d     The unit ID
     */
    public MBAP(short a, short b, short c, byte d){
        tID = a;
        pID = b;
        lenght = c;
        uID = d;
        mbapBB = ByteBuffer.allocate(7);
        mbapBB.putShort(a).putShort(b).putShort(c).put(d);
    }

    /**
     * Public constructor.
     * Create a MBAP using a ByteBuffer.
     * @param bb    The ByteBuffer
     */
    public MBAP(ByteBuffer bb){
        tID = bb.getShort(0);
        pID = bb.getShort(2);
        lenght = bb.getShort(4);
        uID = bb.get(5);
    }

    /**
     * Set the current values of the MBAP
     * @param a     The transaction ID
     * @param b     The protocol ID
     * @param c     The message length
     * @param d     The unit ID
     */
    public void setMBAP(short a, short b, short c, byte d)
    {
        tID = a;
        pID = b;
        lenght = c;
        uID = d;
        mbapBB.putShort(a).putShort(b).putShort(c).put(d);

    }

    /**
     * Getters
     */
    public short getTransID(){
        return tID;
    }
    public short getProtoID(){
        return pID;
    }
    public short getlength(){
        return lenght;
    }
    public byte getUnitID(){
        return uID;
    }

    public ByteBuffer getBB() {
        return mbapBB;
    }


}
