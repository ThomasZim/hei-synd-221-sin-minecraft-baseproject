package ch.hevs.isi.field;

/**
 * Modbus Exception class. Creates
 * an exception (string) using a
 * Modbus error code.
 *
 * @author Pompili
 * @version v1.0
 */
public class ModbusException extends Throwable{
    public String ErrorMes;

    /**
     * Public constructor
     * Retrieves the string corresponding
     * to the provided error code.
     * @param x     The error code
     */
    public ModbusException(int x){ErrorMes = ModbusError(x);}

    /**
     * Return the error string
     * @return  The error string
     */
    public String toString(){return ErrorMes;}

    /**
     * Retrieves the error string corresponding
     * to the error code.
     * @param e     The error code
     * @return      The string representation
     */
    public String ModbusError(int e){

        switch(e){
            case 1:
                ErrorMes = "ILLEGAL FUNCTION";
                return ErrorMes;
            case 2:
                ErrorMes = "ILLEGAL DATA ADDRESS";
                return ErrorMes;
            case 3:
                ErrorMes = "ILLEGAL DATA VALUE";
                return ErrorMes;
            case 4:
                ErrorMes = "SLAVE DEVICE FAILURE ";
                return ErrorMes;
            case 5:
                ErrorMes = "ACKNOWLEDGE";
                return ErrorMes;
            case 6:
                ErrorMes = "SLAVE DEVICE BUSY";
                return ErrorMes;
            case 8:
                ErrorMes = "MEMORY PARITY ERROR";
                return ErrorMes;
            case 10:
                ErrorMes = "GATEWAY PATH UNAVAILABLE";
                return ErrorMes;
            case 11:
                ErrorMes = "GATEWAY TARGET DEVICE FAILED TO RESPOND";
                return ErrorMes;
            default:
                return null;

        }

    }
}
