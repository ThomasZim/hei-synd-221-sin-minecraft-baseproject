package ch.hevs.isi.web;

import ch.hevs.isi.core.BinaryDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * Interacts with a webpage that displays
 * the current values of the Minecraft world
 * and acting on inputs.
 *
 * @author Zimmermann
 * @version 1.3
 */
public class WebConnector extends WebSocketServer implements DataPointListener {
    // Attributes
    private static WebConnector instance;
    private static List<WebSocket> wsList;

    /**
     * Private constructor.
     */
    private WebConnector()
    {
        super(new InetSocketAddress(8888));
        start();
        wsList = new ArrayList<WebSocket>();

    }

    /**
     * Is called on page open
     * @param webSocket         The WebSocket
     * @param clientHandshake   The ClientHandShake
     */
    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake)
    {
    }

    /**
     * Is called on page close
     * @param webSocket     The WebSocket
     * @param i
     * @param s
     * @param b
     */
    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        System.out.println("onClose");
    }

    /**
     * Is called on message received
     * @param webSocket     The WebSocket
     * @param s             The received string
     */
    @Override
    public void onMessage(WebSocket webSocket, String s) {
        String delims ="[=]+";
        String[] tokens = s.split(delims);
        DataPoint.getDataPointFromLabel(tokens[0]).fromString(tokens[1]);
    }

    /**
     * Is called on error
     * @param webSocket     The WebSocket
     * @param e             The error
     */
    @Override
    public void onError(WebSocket webSocket, Exception e) {
        e.printStackTrace();
    }

    /**
     * Is called on start
     */
    @Override
    public void onStart() {
    }

    /**
     * Get the only instance of this class.
     * @return      The only instance
     */
    public static WebConnector getInstance()
    {
        if(instance==null)
        {
            instance = new WebConnector();
        }
        return instance;
    }

    /**
     * Push to the webpage
     * @param label     The datapoint label
     * @param value     Its string value
     */
    private void pushToWebPages(String label, String value)
    {
        broadcast(label + "=" + value);
    }

    /**
     * Is called on Datapoint modification.
     * @param fdp   The modified datapoint
     */
    @Override
    public void onNewValue(FloatDataPoint fdp) {
        pushToWebPages(fdp.getLabel(),Float.toString(fdp.getValue()));
    }

    /**
     * Is called on Datapoint modification.
     * @param bdp   The modified datapoint
     */
    @Override
    public void onNewValue(BinaryDataPoint bdp) {
        pushToWebPages(bdp.getLabel(), Boolean.toString(bdp.getValue()));
    }
}
